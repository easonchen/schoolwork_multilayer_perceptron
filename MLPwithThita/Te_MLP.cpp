// Te_MLP.cpp : 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <conio.h>
#include <string.h>

using namespace std;

#define   Input_file   "in.dat"
#define   Desired_output_file  "out.dat"
#define   Hid_Out_weight "w.dat"
#define   In_Hid_weight "v.dat"
#define   Test_output_file   "test_y.dat"


#define a(f)   1 / (1 + exp(-f))
#define in_varl   2
#define out_varl  1
#define node   10
#define  dat_num    50


void READ_W(FILE *ft);
void READ_V(FILE *ft);
void READ_input_data(FILE *fx);
void READ_output_data(FILE *fy);
void TEST_SAVE(void);
float x[dat_num+1][in_varl+1],d[dat_num+1][out_varl+1]  ;
float y[out_varl+1] ;
float v[node+1][in_varl+1],w[out_varl+1][node+1] ;

int main(int argc, char* argv[])
{
	 FILE *stream ;
 
      if ( fopen_s(&stream, In_Hid_weight,"r") !=0) 
		  { printf("open error 1 ");
	       exit(1); }
      READ_V(stream);
      fclose(stream) ;

     if ( fopen_s(&stream,Hid_Out_weight,"r") !=0) 
		  { printf("open error 2 ");
	       exit(1); }
      READ_W(stream);
      fclose(stream) ;

      if ( fopen_s(&stream,Input_file,"r") !=0) 
	       { printf("open error 3 ");
	       exit(1); }
	  READ_input_data(stream);
      fclose(stream) ;

	 /* if ( fopen_s(&stream,Desired_output_file,"r") !=0) 
	       { printf("open error 4 ");
	       exit(1); }
	  READ_output_data(stream);
      fclose(stream) ; */
	  
	  
	  TEST_SAVE();
	     
      return 0;
}




void READ_W(FILE *ft)
{
  int  i,j ;
  
for(j=0;j< out_varl;j++)
     for(i=0;i< node;i++)
    fscanf_s(ft,"%f ",&w[j][i]) ;
    
}



void READ_V(FILE *ft)
{
 float dat1;
 int  i,j,k ;

for(j=0;j<node;j++)
  for(i=0;i< in_varl;i++)
	  fscanf_s(ft,"%f ",&v[j][i]);	 
}





void READ_input_data(FILE *fx)
{
   int i,j;

 for(i=0; i< dat_num; i++)
     for(j=0;j< in_varl ;j++)
       fscanf_s(fx,"%f",&x[i][j]);  	
	
}


void READ_output_data(FILE *fy)
{
   int i,j;

   ;for (i=0;i<dat_num;i++)
    for(j=0;j< out_varl;j++)
        fscanf_s(fy,"%f ",&d[i][j]);  
 
}



void TEST_SAVE(void)

{  int i,j,t,q,num ;
   FILE *stream ;
   float h[node+1],Y,H ;
   FILE  *fout  ;


   printf("\n   testing : ********************************** \n");
   

  if( fopen_s(&fout, Test_output_file,"w") != 0 ) exit(1) ;

  
  for(num=0;num< dat_num ;num++)
   {
    for(q=0;q< node ;q++)
    { H = 0 ;
      for(i=0;i< in_varl ;i++)
	   H = H + v[q][i]*x[num][i] ;
      h[q] = a(H) ;
    }

   for(j=0;j< out_varl ;j++)
    {  Y = 0 ;
       for(q=0;q< node ;q++)
	   Y = Y  + w[j][q]*h[q] ;
       y[j] = a(Y) ;

       fprintf(fout,"%f  ",y[j] );

    }

      fprintf(fout," \n ");

   }/* end for num */

   fclose(fout) ;

}


