
// MLP with one hidden layer 
//

#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

using namespace std;

#define   Input_file   "in.dat"
#define   Desired_output_file  "out.dat"
#define   Actual_output_file   "act_y.dat"
#define   Hid_Out_weight "w.dat"
#define   Hid_Out_bias "wt.dat"
#define   In_Hid_weight "v.dat"
#define   In_Hid_bias "vt.dat"
#define   RMSE "rmse.dat"

#define  a(f)      1 / (1 + exp(-f))	//sigmoid function
#define  eta       0.11					// learning constant0.11
#define  afa       0.2					// momentum constant 0.2
#define  in_varl   2					// number of input variables
#define  out_varl  1					// number of output variables
#define  node      10					// number of hidden layer nodes
#define  dat_num   50					// number of training samples
 #define  _iteration 30000				// number of learning iterations

void Read_data(void) ;    // read training data pairs
void LEARN(void) ;        // learning the weights
void TEST(void) ;         // compute the actual outputs
void SAVE(void) ;         // save the learned weights




float  x[dat_num+1][in_varl+1],d[dat_num+1][out_varl+1],y[out_varl+1] ;
float  v[node+1][in_varl+1],w[out_varl+1][node+1];
float  vt[node + 1][in_varl + 1], wt[out_varl + 1][node + 1], e[_iteration + 1][out_varl + 1];	//易聖


int main(int argc, char* argv[])
{
	
 time_t t;


   srand((unsigned) time(&t));

   Read_data();
   LEARN();
   TEST();
   SAVE() ;
   printf("\n  ******   The  end " );
   //system("Pause");	//易聖
	
   return 0;
}



void Read_data()

{
  int  i,j ;

  FILE *fm ;

 if (fopen_s(&fm,Input_file,"r") != 0) exit(1);				//開啟檔案讀取輸入
    
  for (i=0;i<dat_num;i++)
    for(j=0;j<in_varl;j++)
      fscanf_s(fm,"%f ",&x[i][j]) ;							//將輸入存成x(第i筆資料,第j個輸入)
 fclose(fm) ;	   


 if (fopen_s(&fm,Desired_output_file,"r") != 0) exit(1);	//開啟檔案讀取理想輸出

 for (i=0;i<dat_num;i++)
    for(j=0;j< out_varl;j++)
        fscanf_s(fm,"%f ",&d[i][j]) ;						// 將理想輸出存成d(第i筆資料,第j個理想輸出)
 
 fclose(fm) ;	   


}



void LEARN()

{
  int i,j,q,num,itera = 0;
  float H,Y,sum, *error,*h,*dely,*delh, *difference ;
  float  (*apv)[in_varl+1],(*apw)[node+1];
  float(*apvt)[in_varl + 1], (*apwt)[node + 1];	//易聖

  
  if ( !( error = new float[out_varl+1] ) ||
	   !( h = new float[node+1] ) ||
       !( dely = new float[out_varl+1] ) ||
       !( delh = new float[node+1] ) ||
	   !( difference = new float[out_varl+1] ) ||
       !( apv = new float[node+1][in_varl+1]  ) ||
       !( apw = new float[out_varl+1][node+1]  ) ||
	   !(apvt = new float[node + 1][in_varl + 1]) ||	//易聖
	   !(apwt = new float[out_varl + 1][node + 1]) )	//易聖
  {  cout << "\n Insufficient memory for learn" ;
     exit(1) ;
  }


   for(q=0;q<node;q++)
     for(i=0;i<in_varl ;i++)
	{ v[q][i] = (rand()%(1001)-500)/10000.  ;		//隱藏層權值初始值(範圍-0.05~0.05)
      vt[q][i] = (rand() % (1001) - 500) / 10000.;	//隱藏層偏權值初始值(範圍-0.05~0.05)(易聖)
	  apv[q][i] = 0 ;								//隱藏層權值更新量初始值
	  apvt[q][i] = 0;								//隱藏層偏權值更新量初始值
	 }

  for(j=0;j< out_varl;j++)
     for(q=0;q< node ;q++)
	{ w[j][q] = (rand()%(1001)-500)/10000.0 ;		//輸出層權值初始值(範圍-0.05~0.05)
      wt[j][q] = (rand() % (1001) - 500) / 10000.0;	//輸出層偏權值初始值(範圍-0.05~0.05)(易聖)
	  apw[j][q] = 0 ;								//輸出層權值更新量初始值
	  apwt[j][q] = 0;								//輸出層偏權值更新量初始值(易聖)
	 }


while(itera < _iteration)						//疊代次數
  {
   for(j=0;j< out_varl;j++)
	  error[j] = 0 ;							//Square Error
   
   itera = itera + 1 ;

 for(num=0;num< dat_num ;num++)					//每次疊代跑過全部的資料數
 {
    for(q=0;q<  node;q++)
    { H = 0 ;
      for(i=0;i< in_varl ;i++)
	{ H = H  + v[q][i]*x[num][i] ;				//對輸入資料作加權
	}
	h[q] = a(H- vt[q][i]) ;								//隱藏層輸出(易聖)
    }

   for(j=0;j< out_varl ;j++)
    {  Y = 0 ;
       for(q=0;q< node ;q++)
	  { Y =Y + w[j][q]*h[q] ;								//對隱藏層的輸出作加權
	  }
       y[j] = a(Y- wt[j][q]) ;										//輸出層輸出(易聖)
       difference[j] = d[num][j] - y[j] ;					//預測誤差
	   dely[j] = difference[j] * (1 - y[j])*y[j];			//預測誤差乘激活函數對輸出層加權輸入的微分(尚未乘上一層的輸出)
       error[j] = error [j] + difference[j]*difference[j];	//隨著疊代次數累加預測誤差平方       
    }

    for(q=0;q< node ;q++)
    {
       sum = 0 ;
       for(j=0;j< out_varl ;j++)
	{ sum = sum + dely[j]*w[j][q] ;	//把所有輸出變數的dely乘上每個隱藏層神經元的權值加總起來
	}
	delh[q] = h[q]*(1 - h[q])*sum ;	//乘上隱藏層神經元的激活函數微分(h[q]*(1 - h[q]))
     }

     for(j=0;j< out_varl ;j++)
       for(q=0;q< node;q++)
	 {
	   apw[j][q] = eta*dely[j]*h[q] +afa*apw[j][q] ;	//輸出層權值更新量
	   w[j][q] = w[j][q] + apw[j][q] ;					//輸出層權值
	   apwt[j][q] = eta*dely[j]+ afa*apwt[j][q];		//輸出層偏權值更新量(易聖)
	   wt[j][q] = wt[j][q] + apwt[j][q];				//輸出層偏權值(易聖)
	 }


     for(q=0;q< node;q++)
       for(i=0;i< in_varl ;i++)
	  {
	    apv[q][i] = eta*delh[q]*x[num][i]  + afa*apv[q][i] ;	//隱藏層層權值更新量
	    v[q][i] =  v[q][i] +  apv[q][i] ;						//隱藏層層權值
		apvt[q][i] = eta*delh[q]+ afa*apvt[q][i];	//隱藏層偏權值更新量(易聖)
		vt[q][i] = vt[q][i] + apvt[q][i];						//隱藏層偏權值(易聖)
	   }

    }


    if ((itera %1 ) == 0 )
      { printf("\n itera is %d, RMSE of  ",itera);
	     for(j=0;j< out_varl ;j++)
	    printf(" output %d is --- %f ", j+1, e[itera-1][j] = sqrt(error[j]/dat_num));	//易聖
	}

    }/* end while */


  delete [] error ;
  delete [] h ;
  delete [] dely ;
  delete [] delh ;
  delete [] difference ;
  delete [] apv ;
  delete [] apw ;


 }




void TEST()

{  int i,j,num,q ;
   float h[node+1],H,Y,error[out_varl+1],difference[out_varl+1]; 
   FILE *fout  ;


  if( fopen_s(&fout, Actual_output_file,"w") !=0 ) exit(1) ;

  
   for(j=0;j< out_varl ;j++)
	   error [j]=0 ;
  
  for(num=0;num< dat_num ;num++)
   {
     for(q=0;q< node;q++)
    { H = 0 ;
      for(i=0;i< in_varl ;i++)
	   H = H + v[q][i]*x[num][i] ;
       h[q] = a(H- vt[q][i]) ;	//易聖
    }

   for(j=0;j< out_varl ;j++)
    {  Y = 0 ;
       for(q=0;q< node;q++)
	   Y = Y + w[j][q]*h[q] ;
       y[j] = a(Y- wt[j][q]) ;	//易聖

       fprintf(fout,"%f   \n",y[j] );
  
       difference [j] = d[num][j] - y[j]  ;
       error [j] = error [j] + difference [j]*difference [j]  ;
    }
    
     
   }/* end for num */

        printf("\n\n ++++ Final result: ");
	     for(j=0;j< out_varl ;j++)
	    printf(" output %d is --- %f ", j+1, sqrt(error[j]/dat_num));
  
 
   fclose(fout) ;

}



void SAVE()
{

FILE *fp ;
int i,j, q,k;

if ( fopen_s(&fp, In_Hid_weight,"w") !=0 ) exit(1) ;

for(q=0;q< node;q++)
  { for(i=0;i< in_varl ;i++)
	fprintf(fp,"%f ",v[q][i]) ;
   fprintf(fp,"\n");
  }

 fclose(fp) ;

 if (fopen_s(&fp, In_Hid_bias, "w") != 0) exit(1);	//易聖

 for (q = 0; q< node; q++)
 {
	 for (i = 0; i< in_varl; i++)
		 fprintf(fp, "%f ", vt[q][i]);
	 fprintf(fp, "\n");
 }

 fclose(fp);
 
if ( fopen_s(&fp, Hid_Out_weight,"w") !=0 ) exit(1) ;

 for(j=0;j< out_varl ;j++)
   {  for(q=0;q< node;q++)
      fprintf(fp,"%f ", w[j][q]) ;
      fprintf(fp,"\n");
   }

  fclose(fp) ;

  if (fopen_s(&fp, Hid_Out_bias, "w") != 0) exit(1);	//易聖

  for (j = 0; j< out_varl; j++)
  {
	  for (q = 0; q< node; q++)
		  fprintf(fp, "%f ", wt[j][q]);
	  fprintf(fp, "\n");
  }

  fclose(fp);
  
//易聖
 if (fopen_s(&fp, RMSE, "w") != 0) exit(1) ;

  for (k = 0; k< _iteration; k++)
  {	  for (j = 0; j< out_varl; j++)
		  fprintf(fp, "%f ", e[k][j]);
	  fprintf(fp, "\n");
  }

  fclose(fp);

}
