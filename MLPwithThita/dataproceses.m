close all;clear;clc;
input=load('IN.DAT');output=load('OUT.DAT');
for i=1:5
    ActualOutput=['act_y' int2str(i) '.dat'];y(:,i)=load(ActualOutput);
    ActualOutputWithThita=['act_yt' int2str(i) '.dat'];yt(:,i)=load(ActualOutputWithThita);
    RMSE=['rmse' int2str(i) '.dat'];r(:,i)=load(RMSE);
    RMSEWithThita=['rmset' int2str(i) '.dat'];rt(:,i)=load(RMSEWithThita);    
end
tyt=load('test_y.dat');
rmseL001=load('rmseL0.01.dat');
rmseL1=load('rmseL1.dat');
rmseL10=load('rmseL10.dat');
rmseA04=load('rmseA0.4.dat');rmseA06=load('rmseA0.6.dat');
rmseA08=load('rmseA0.8.dat');rmseA1=load('rmseA1.dat');

figure;plot(1:length(output),output);hold on;
plot(1:length( y), y,'g');
plot(1:length( yt), yt,'r');
% plot(1:length(tyt), tyt,'k');

figure;plot(1:length(r),log10(r),'b');hold on;
plot(1:length(rt),log10(rt),'r');

figure;plot(1:length(rmseL001),log10(rmseL001));hold on;
plot(1:length(rt(:,1)),log10(rt(:,1)));plot(1:length(rmseL1),log10(rmseL1));
plot(1:length(rmseL10),log10(rmseL10));

figure;plot(1:length(rt(:,1)),log10(rt(:,1)));hold on;
plot(1:length(rmseA04),log10(rmseA04));plot(1:length(rmseA06),log10(rmseA06));
plot(1:length(rmseA08),log10(rmseA08));plot(1:length(rmseA1),log10(rmseA1));